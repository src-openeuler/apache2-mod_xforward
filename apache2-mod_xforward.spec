%define apache_libexecdir %(apxs -q LIBEXECDIR)

Name:           apache2-mod_xforward
Version:        0.6
Release:        lp151.1.6
Summary:        Apache module implementing handling of X-FORWARD headers
License:        Apache-2.0
Url:            https://github.com/openSUSE/apache2-mod_xforward/
Source:         https://github.com/openSUSE/%{name}/archive/master.zip

BuildRequires:  httpd-devel gcc perl
Requires:       httpd 

%description
The xforward module implements internal redirection based on X-Forward response header.
When this header is seen by Apache it uses the apache mod_proxy module to proxy the
given URL.

%prep
%setup -n %{name}-master

%build
apxs -c mod_xforward.c

%install
mkdir -p $RPM_BUILD_ROOT/%{apache_libexecdir}
cp -p .libs/mod_xforward.so $RPM_BUILD_ROOT/%{apache_libexecdir}
mkdir -p %{buildroot}%{_httpd_modconfdir}
echo "LoadModule xforward_module modules/mod_xforward.so" > %{buildroot}%{_httpd_modconfdir}/10-xforward.conf

%files
%defattr(-,root,root)
%{apache_libexecdir}/*.so
%{_httpd_modconfdir}/10-xforward.conf

%changelog
* Thu Jun 20 2024 Wenlong Zhang <zhangwenlong@loongson.cn> - 0.6-lp151.1.6
- add config for mod_xforward

* Fri Jan 14 2022 Ge Wang <wangge20@huawei.com> - 0.6-lp151.1.5
- add buildrequire perl

* Wed Jun 30 2021 zhouwenpei <zhouwenpei1@huawei.com> - 0.6-lp151.1.4
- add buildrequire gcc

* Thu Sep 10 2020 Ge Wang <wangge20@huawei.com> - 0.6-lp151.1.3
- Modify Source0 Url

* Mon Mar  2 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.6-lp151.1.2
- Package init
